package pl.teamkiwi.domain.model.exception

open class ConflictException(
    message: String
) : Exception(message)