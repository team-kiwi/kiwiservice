package pl.teamkiwi.domain.model.exception

class NoContentException : Exception()