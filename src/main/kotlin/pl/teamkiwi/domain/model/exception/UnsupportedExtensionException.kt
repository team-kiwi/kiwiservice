package pl.teamkiwi.domain.model.exception

open class UnsupportedExtensionException(
    message: String
) : Exception(message)